// 
// Test 2 Part B Debug Program
//   Find the 6 errors
#include <iostream>
using namespace std;

class CPolygon {
protected:
	int width, height;
public:
	void set_values(int a, int b)
	{
		width = a; height = b;
	}
};

class COutput {
public: //DEBUG 03 - Added public tag
	void output(int i);
};

void COutput::output(int i) {
	cout << i << endl; //DEBUG 01 - Fixed typo
}

class CRectangle : public CPolygon, public COutput {
public:
	int area()
	{
		return (width * height);
	}
};

class CTriangle : public CPolygon, public COutput { //DEBUG 04 - Added 'public COutput'
public:
	int area()
	{
		return (width * height / 2);
	}
};

// Function to hold screen open before any exit.
void holdscreen()
{
	char holdscr;
	cout << "\n\n\t Enter 1 character then hit return to exit program.  ";
	cin >> holdscr;
	return;
}


int main() {
	CRectangle rect;
	CTriangle trgl;

	// First set of Values
	cout << "\n\nFirst set of values are 4 and 5 -- Results are: \n";
	rect.set_values(4, 5);
	trgl.set_values(4, 5); //DEBUG 02 - fixed missing comma
	rect.output(rect.area());
	trgl.output(trgl.area());

	// Second set of Values
	cout << "\n\nSecond set of values are 10 and 3 -- Results are: \n;"; //DEBUG 05 - Added missing ';'
	rect.set_values(10, 3);
	trgl.set_values(10, 3);
	rect.output(rect.area());
	trgl.output(trgl.area());

	// third set of Values
	cout << "\n\nThird set of values are 7 and 7 -- Results are: \n";
	rect.set_values(7, 7);
	trgl.set_values(7, 7);
	rect.output(rect.area());
	trgl.output(trgl.area()); //DEBUG 06 - Fixed typo

	// Hold window open to see results  
	holdscreen();

	return 0;
}

